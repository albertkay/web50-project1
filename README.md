# Project 1 Notes
## Web Programming with Python and JavaScript

### Application Notes

There are several key Flask files / directories, described below. 

- `application.py` is the core of the application. It 1) defines Flask 
  routes and 2) prepares HTTP responses to any requests sent via these
  routes
- `query.py` contains SQL queries commonly referenced by application.py
- the directories `templates` and `static` are primarily responsible
  for the presentation layer (via HTML or JSON)
- `databases.sql` contains the SQL that creates the Postgres databases
- `import.py` populates the database with data

### Database Notes

Four entities are modeled in the database via four tables - *titles*,
*comments*, *authors*, and *users* (i.e. users of the system or web app).
The key database relationships are as follows ..

- one title can have multiple comments
- one title can have multiple authors (i.e. a title can have one or more
  authors)
- one author can have multiple titles (i.e. one or more titles)
- one comment can be associated with one and only one user and with one and
  only one title

### Database Setup

Heroku was used initially, but after deciding that a many to many
relationship was required to track titles and authors, an additional
junction table was needed to enforce this, causing the 10K row limit 
imposed by Heroku to be exceeded (and generating warnings about 
database operations and access being adversely affected). 

Follow the steps below to set up a database instance running
outside of Heroku.

- Within Postgres, create a database
```
  postgres=# create database mybooks;
  CREATE DATABASE
  postgres=#
```
- Execute the SQL code in databases.sql to create all the required tables
  on this Postgres database instance. For example
```
  postgres=# \c mybooks
  You are now connected to database "mybooks" as user "xyz".
  mybooks=# create table titles (
  mybooks(#   id serial primary key,
  mybooks(#   isbn varchar unique not null,
  mybooks(#   title varchar not null,
  mybooks(#   pubdate integer not null
  mybooks(# );
  CREATE TABLE
  postgres=#
```
- Within a shell/command prompt set the environment variable `DATABASE_URL`
  to point to this newly created Postgres instance. In the example below the
  active instance happens to be hosted locally on my machine
```
  $ export DATABASE_URL=postgres://localhost/mybooks
```
- Within the same command prompt install all the required packages listed in
  `requirements.txt` via `pip` (if this hasn't been done already)

- Still within the command prompt, import CSV data into the database by
  executing the following Python3 script `import.py`
```
  $ python import.py authors
  $ python import.py titles
  $ python import.py junction
```
That's it - the script should run cleanly, and the database is now set up and
populated with data :)

### Application Startup

- Assuming `pip` has accounted for all dependencies specified in `requirements.txt`,
  start the Flask server
```
  $ export DATABASE_URL=postgres://localhost/mybooks
  $ export FLASK_APP=application.py
  $ flask run
```

### Miscellaneous

Python 3.6.4 and pip 9.0.1 and Postgres 10.2 were used for development and
testing.
```
  (flask_prov) xxxx $ python -V
  Python 3.6.4
  (flask_prov) xxxx $ pip -V
  pip 9.0.1 from /Users/xxxx/.virtualenvs/flask_prov/lib/python3.6/site-packages (python 3.6)
  (flask_prov) xxxx $ 
```
```
  postgres=# select version();
                                                      version                                                    
  ---------------------------------------------------------------------------------------------------------------
   PostgreSQL 10.2 on x86_64-apple-darwin17.3.0, compiled by Apple LLVM version 9.0.0 (clang-900.0.39.2), 64-bit
  (1 row)

  postgres=# 
```

No attribution was required for banner photo and no animals were harmed during
the construction of this site.
