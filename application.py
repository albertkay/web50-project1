import os
import sys

import requests
import logging
from logging.handlers import RotatingFileHandler
from passlib.hash import pbkdf2_sha256
from flask import Flask, jsonify, render_template, redirect, request, session, url_for
from flask_session import Session
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

import query

app = Flask(__name__)

# Check for environment variable
if not os.getenv("DATABASE_URL"): raise RuntimeError("DATABASE_URL is not set")

# Configure session to use filesystem
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"

# Configure a secret key for sessions
app.secret_key = os.urandom(24)

# Configure logging - https://gist.github.com/ibeex/3257877
handler = RotatingFileHandler('server.log', maxBytes=64000, backupCount=3)
handler.setLevel(logging.INFO)
app.logger.addHandler(handler)

Session(app)

# Set up database
engine = create_engine(os.getenv("DATABASE_URL"))
db = scoped_session(sessionmaker(bind=engine))


GOODREADS_API_KEY = 'tsI8dzrDmWrjSZCcsF5cyg'  # API key
GOODREADS_API_URL = 'https://www.goodreads.com/book/review_counts.json'
GOODREADS_TIMEOUT = 3                         # API timeout in seconds
LOGGED_IN_KEY     = 'user_logged_in'          # Login session key
LOGGED_IN_USER    = 'username'                # username of logged in user
LOGIN_ERROR       = 'auth_error'


# Handle user logout requests

@app.route("/logout", methods = ['GET'])
def logout():

    _user_logged_in(False)
    return redirect(url_for('login'))


# Handle user authentication / login requests

@app.route("/login", methods = ['GET','POST'])
def login():

    if request.method == 'GET': 

        return render_template('_login.html', intro=True)

    if request.method == 'POST':

        username  = request.form.get('username')
        password  = request.form.get('password')
        operation = request.form.get('operation')

        if operation == 'register': 
            
            signup_success = _create_user_with(username, password)
            return render_template('_login.html', signup_success=signup_success)

        if operation == 'signin':

            if _user_can_authenticate_with(username, password):
                _user_logged_in(True)
                _current_user(username)
                return render_template('_generic.html', login_success=True)

            else:
                return render_template('_login.html', auth_failure=True)


# Query database for list of titles matching search term and
# displays results.
#
# Search can be done against title, author, or ISBN

@app.route("/", methods = ['GET','POST'])
def index():

  # Look using possibility with decorators for next iteration :)
    if _user_logged_in() == False: return redirect(url_for('login'))

    authors = {}  # Authors associated with a book { id: [ author ] }
    books = []    # list of books returned from query

    if request.method == 'POST':

        search_type = request.form.get("bookattribute")
        search_text = request.form.get("searchtext")
        query_meta  = { 
            search_type  : True,
            'details'    : url_for('details'),
            'searchtext' : search_text,
        }
        search_text = search_text.upper()

      # Weed out invalid search strings - probably should do this in Javascript
        if not _can_do_query(search_text): 
            
            return render_template('_generic.html') # Do nothing

      # Perform actual search/query and begin preparing an HTML response :)
        if search_type == 'books_via_title' or search_type == 'books_via_isbn':

            books = _search_database(search_text, search_type)
            for book in books:
                authors[book['id']] = _get_authors_of_book(book['id'])

        if search_type == 'books_via_author':

            author_records = _get_author_ids(search_text)
            for record in author_records: # Handle multiple authors as needed
                author_id = str(record['id'])
                books = books + _search_database(author_id, search_type)

        return render_template('_result.html',
                                books = books,
                                authors = authors,
                                query_meta = query_meta)

    else:

        return render_template('_generic.html')


# Query database for details of a specific book with matching ISBN
# and displays results

@app.route("/book", methods = ['GET'])
def details():

    if _user_logged_in() == False: return redirect(url_for('login'))

    want_multiple_records = False
    isbn_string = request.args.get('isbn', '')
    book = _search_database(isbn_string, 'books_via_isbn', want_multiple_records)
    authors = _get_authors_of_book(book['id'])

  # Make book information as part of application state for potential
  # use in comment submission
    session['isbn_query_result'] = book
    session['authors'] = authors

    search_text = str(book['id'])
    search_type = 'comments_via_book'
    comments = _search_database(search_text, search_type)

  # Get review info using 3rd party API
    review_info = _get_goodreads_ratings_for(book['isbn'])

    return render_template('_detail.html',
                            book = book,
                            authors = authors,
                            comments = comments, 
                            review_info = review_info
    )


# Handle user comment form display and form submission 

@app.route("/comment", methods = ['GET','POST'])
def comments():

  # Retrieve book information from application state
    results = session['isbn_query_result']
    authors = session['authors']

    if request.method == 'GET':

        return render_template('_comment.html', record=results, authors=authors)

    else:

        user_record = _search_database(_current_user(), 'users', False)
        comments = _search_database(str(results['id']), 'comments_via_book')

        for comment in comments: # See if user has already submitted comment
            if comment['user_id'] == user_record['id']:
                return render_template('_comment.html', 
                            submitted=True, record=results, authors=authors)

        title = request.form.get('title').strip()
        comment = request.form.get('comment').strip()
        rating = request.form.get('rating')

        if not title or not comment or not rating:
            return render_template('_comment.html', 
                            record=results, authors=authors, incomplete=True)

        query_parameters = {
            "title_id" : results['id'],
            "title"    : title, 
            "content"  : comment,
            "rating"   : int(rating),
            "user_id"  : user_record['id'],
        }

        _write_database('comment', query_parameters)
        
        return redirect(url_for('details', isbn=results.isbn, authors=authors))


# Queries database for book with ISBN <isbn> and if found prepares 
# a response in JSON format

@app.route("/isbn/<string:isbn>", methods=['GET'])
def details_in_json(isbn):

    book = _get_book_with_isbn(isbn)

    if book == None:
        return jsonify(text=f'No book with ISBN {isbn} found'), 404

    authors = []
    for a in _get_authors_of_book(book['id']): authors.append(a['fullname'])

  # Get review info using 3rd party API
    review_info = _get_goodreads_ratings_for(book['isbn'])

    if review_info: 

        return jsonify(title=book['title'], 
                        year=book['pubdate'],
                        author=authors,
                        isbn=book['isbn'], 
                        review_count=review_info['work_ratings_count'],
                        average_score=float(review_info['average_rating'])), 200

    else:

        return jsonify(title=book['title'], 
                        year=book['pubdate'],
                        author=authors,
                        isbn=book['isbn'], 
                        review_count='N/A',
                        average_score='N/A'), 200


# Catchall function to deal with invalid URLs

@app.errorhandler(404)
def page_not_found(error):
    
    return render_template('_missing.html'), 404
    

def _write_database(key, values):

    query_parameters = { key : values }

    try:
        db.execute(query.insertions[key], query_parameters[key])
        db.commit()

    except:
        db_exception_object = sys.exc_info()[0]
        app.logger.error("insert database '%s' generated exception %s, %s, %s",
                            key,
                            request.remote_addr, 
                            request.url, 
                            db_exception_object)
        return False

    else:
        return True


def _search_database(search_text, key, fetch_multiple_rows=True):

    query_parameters = {
        'authors_of_book'  : { "search_token" : search_text },
        'books_via_title'  : { "search_token" : '%' + search_text + '%' },
        'books_via_isbn'   : { "search_token" : '%' + search_text + '%' },
        'books_via_author' : { "search_token" : search_text },
        'book_via_isbn'    : { "search_token" : search_text },
        'comments_via_book': { "search_token" : search_text },
        'authors'          : { "search_token" : '%' + search_text.upper() + '%' },
        'users'            : { "search_token" : search_text },
    }

    try:

        results = db.execute(
                    query.searches[key],
                    query_parameters[key])

    except:

        db_exception_object = sys.exc_info()[0]
        app.logger.error("query '%s' generated exception %s, %s, %s",
                            key,
                            request.remote_addr,
                            request.url,
                            db_exception_object)
        results = None

    else:

        if (fetch_multiple_rows == True):
            return results.fetchall()

        else:
            return results.fetchone()            


def _password_format_ok(password):

  # Can add additional checks as needed
    if len(password) == 0: # Duh don't let this happen

        return False

    return True


def _create_user_with(username, password):

  # Check for duplicate usernames and make sure password format is valid

    if not username:

        return False

    if not _password_format_ok(password): 
        
        return False

    if _search_database(username, 'users', False): 
        
        return False

    hashed = pbkdf2_sha256.hash(password)
    query_parameters = {
        "email": username,
        "password": hashed
    }

    return _write_database('user', query_parameters)


def _get_authors_of_book(from_book_id):

    return _search_database(str(from_book_id), 'authors_of_book')


def _get_author_ids(from_author_name):

    return _search_database(from_author_name, 'authors')


def _get_book_with_isbn(isbn):

    return _search_database(isbn, 'book_via_isbn', False)


def _can_do_query(with_search_text):

    if len(with_search_text) == 0 or '%' in with_search_text:
        return False

    else:
        return True


# Queries session state and returns if user is logged in or not - or -
# Otherwise sets logged in flag as specified in arg and clears session state
# (recommend use this set feature only upon login or logout)

def _user_logged_in(flag=None):

    if flag == None:

        if not LOGGED_IN_KEY in session: session[LOGGED_IN_KEY] = False
        return session[LOGGED_IN_KEY]

    else:

        session.clear() # Clear out application state on login and logout 
        session[LOGGED_IN_KEY] = flag


def _current_user(username=None):

    if username == None:
        return session[LOGGED_IN_USER]
    
    else:
        session[LOGGED_IN_USER] = username


def _get_goodreads_ratings_for(isbn):

    query_parameters = {
        'key'  : GOODREADS_API_KEY, 
        'isbns': isbn
    }

    try:
        res = requests.get(GOODREADS_API_URL, 
                            params=query_parameters, 
                            timeout=GOODREADS_TIMEOUT)

    except:
        review_info = None

    else:
        review_info = res.json()['books'][0]        

    return review_info


def _user_can_authenticate_with(username, password):

    want_multiple_records = False
    user = _search_database(username, 'users', want_multiple_records)

    passwds_match = (user and pbkdf2_sha256.verify(password, user['passwd']))

    if not passwds_match: 
        app.logger.warning('login failed with user %s, URL %s, addr %s',
                    username, request.url, request.remote_addr)

    return passwds_match
