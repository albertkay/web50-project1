import csv
import os
import sys

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

engine = create_engine(os.getenv("DATABASE_URL"))
db = scoped_session(sessionmaker(bind=engine))

# Parses CSV and return unique list of authors

CSVFILE = 'books.csv'

def _getAuthors(csvfile):

    authorList = []

    f = open(csvfile)
    reader = csv.reader(f)
    next(reader)

    for r_isbn, r_title, r_author, r_pubdate in reader:

        # Be able to handle book with multiple authors edge case
        book_authors = r_author.split(',')
        for a in book_authors: authorList.append(a.strip())

        # Create function that strips extra whitespace from author name's
        # string for better formatting consistency 
        # https://tinyurl.com/y7z3wtge
        clearExtraSpace = lambda x: ' '.join(x.split())

    return set(map(clearExtraSpace, authorList))


def stockImport(csvfile):

    f = open(sys.argv[1])
    reader = csv.reader(f)
    next(reader)

    records_added = 0

    for isbn, title, author, pubdate in reader:
        db.execute("INSERT INTO origtitles (isbn, title, pubdate, author) VALUES (:isbn, :title, :pubdate, :author)",
                    {"isbn": isbn, "title": title, "pubdate": pubdate, "author": author})
        print(f"Added {records_added} book titled {title} {author} published {pubdate} {isbn}")
        records_added = records_added + 1

    db.commit()


def populateJunction(csvfile):

    f = open(csvfile)
    reader = csv.reader(f)
    next(reader)

    records_added = 0

    for raw_isbn, raw_title, raw_author, raw_pubdate in reader:
      # Break out multiple authors from one book, if necessary
        book_authors = raw_author.split(',')
        for author in book_authors:
            db.execute(
                """ INSERT INTO 
                    author_title (title_id, author_id) 
                    VALUES (
                        ( select id from titles
                          where title = :title and isbn = :isbn),
                        ( select id from authors where fullname = :author )
                    )""",
                {   "author": ' '.join(author.split()),
                    "title": raw_title, 
                    "isbn": raw_isbn
                }
            )
            records_added = records_added + 1
            print(f"Added {records_added} {raw_title} {author}")

    db.commit()


def populateTitles(csvfile):

    f = open(csvfile)
    reader = csv.reader(f)
    next(reader)

    records_added = 0

    for isbn, title, author, pubdate in reader:
        db.execute(
            """INSERT INTO titles (isbn, title, pubdate) 
               VALUES (:isbn, :title, :pubdate)""",
            { "isbn" : isbn, "title" : title, "pubdate" : pubdate }
        )
        records_added = records_added + 1
        print(f"Added {records_added} book titled {title} published {pubdate} isbn {isbn}")
    db.commit()


def populateAuthors(csvfile):

    authors = _getAuthors(csvfile)

    records_added = 0

    for author in authors:
        db.execute("INSERT INTO authors (fullname) VALUES (:author)",
                    {"author": author})
        records_added = records_added + 1
        print(f"Added {records_added} '{author}'")
    db.commit()


if __name__ == "__main__":

    if len(sys.argv) < 2:

        print(f'python import.py authors|titles|junction')
        sys.exit()

    elif sys.argv[1] == 'authors':

        print(f'Importing authors...')
        populateAuthors(CSVFILE)

    elif sys.argv[1] == 'titles':

        print(f'Importing titles...')
        populateTitles(CSVFILE)

    elif sys.argv[1] == 'junction':

        print(f'Populating junction table...')
        populateJunction(CSVFILE)

    else:

        print(f'python import.py authors|titles|junction')
