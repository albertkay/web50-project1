# Define commonly used SQL queries used to retrieve books by title, isbn,
# and name
#
# Many to many query source - https://tinyurl.com/y9nv28ex

searches = {

    'authors_of_book':
                    """
                    SELECT
                        author_title.author_id, authors.fullname
                    FROM
                        author_title
                    JOIN
                        authors
                    ON
                        author_title.author_id = authors.id
                    WHERE
                        author_title.title_id = :search_token
                    """,

    'books_via_isbn':
                    """
                    SELECT
                        titles.id, titles.title, titles.isbn, titles.pubdate, authors.fullname
                    FROM
                        titles, authors, author_title
                    WHERE
                        upper(isbn) LIKE :search_token
                    AND
                        author_title.title_id = titles.id
                    AND
                        author_title.author_id = authors.id
                    ORDER BY
                        titles.isbn
                    """,

    'books_via_title': 
                    """
                    SELECT
                        titles.id, titles.title, titles.isbn, titles.pubdate
                    FROM
                        titles, authors, author_title
                    WHERE
                        upper(title) LIKE :search_token
                    AND
                        author_title.title_id = titles.id
                    AND
                        author_title.author_id = authors.id
                    GROUP BY
                        titles.title, titles.id
                    ORDER BY
                        titles.title
                    """,

    'books_via_author':
                    """
                    SELECT
                        authors.fullname, titles.title, titles.isbn, titles.pubdate
                    FROM
                        authors, titles, author_title 
                    WHERE
                        authors.id = :search_token
                    AND
                        author_title.author_id = :search_token
                    AND 
                        author_title.title_id = titles.id
                    ORDER BY
                        authors.fullname
                    """,

    'book_via_isbn':
                    """
                    SELECT
                        *
                    FROM
                        titles
                    WHERE
                        titles.isbn = :search_token
                    """,

    'comments_via_book': 
                    """
                    SELECT
                        comments.title,
                        comments.content,
                        comments.rating,
                        to_char(comments.submit_date, 'Month DD, HH24:MI') as submit_date,
                        titles.id,
                        users.id as user_id, users.email
                    FROM
                        comments
                    JOIN
                        titles
                    ON
                        comments.title_id = titles.id
                    JOIN
                        users
                    ON
                        comments.user_id = users.id
                    WHERE
                        titles.id = :search_token
                    ORDER BY
                        submit_date DESC
                    """,

    'authors':      
                    """
                    SELECT 
                        *
                    FROM
                        authors 
                    WHERE
                        upper(fullname)
                    LIKE
                        :search_token
                    ORDER BY 
                        fullname
                    """,

    'users':        
                    """
                    SELECT
                        *
                    FROM
                        users 
                    WHERE
                        email = :search_token
                    """
}

insertions = {
    'user':
                """
                INSERT 
                    into users (email, passwd) 
                VALUES 
                    (:email, :password)
                """,

    'comment':
                """
                INSERT 
                    into comments (title_id, title, content, rating, user_id, submit_date) 
                VALUES 
                    (:title_id, :title, :content, :rating, :user_id, NOW())
                """
}
