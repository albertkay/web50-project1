create table titles (
  id serial primary key,
  isbn varchar unique not null,
  title varchar not null,
  pubdate integer not null
);

create table authors (
  id serial unique primary key,
  fullname varchar unique
);

create table author_title (
  author_id integer not null references authors,
  title_id integer not null references titles
);

create table users (
  id serial unique primary key,
  passwd varchar,
  email varchar
);

create table comments (
  id serial unique primary key,
  title varchar,
  content varchar,
  rating integer,
  title_id integer references titles,
  user_id integer references users,
  submit_date timestamp with time zone
);
